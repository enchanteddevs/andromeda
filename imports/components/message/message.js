import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './../conversation/conversation';
import './../conversation-mobile/conversation-mobile';
import './message-nav/message-nav';

import './message.html';

let ActiveMessageContact = new ReactiveVar(null);

Template.message.onCreated(()=>{
  subscribeToMessage();
});

Template.message.onRendered(()=>{
  Tracker.afterFlush(()=>{
    let _someItem = $('.message-area .content');
    _someItem.scrollTop(_someItem.prop("scrollHeight"));
  });
});

Template.message.helpers({
  activeMessage: ()=>{
    return ActiveMessage.get();
  },

  activeMessageContact: () =>{
    if(ActiveMessage.get()){
      let _contact_name = getConversationWith(ActiveMessage.get()[0].parties);

      return _contact_name;
    }
  },

  messages: () =>{
    if(ActiveMessage.get()){
      return Message.find({conversation_id: ActiveMessage.get()[0]._id},{sort:{timestamp: 1}}).fetch();
    }
  }
});

Template.message.events({
  'click .message-send-btn'(event){
    sendDirectMessage();
  },

  'keypress .message-input'(event) {
    if (event.which === 13) {
      sendDirectMessage();
    }
  },

  'click #message-extended-menu'(event){
    let _message_nav = document.getElementsByClassName('message-navigation');
    console.log(_message_nav[0].style.display);
    if(_message_nav[0].style.display != 'block'){
      _message_nav[0].style.display = 'block';
    }else{
      _message_nav[0].style.display = 'none';
    }
  }
});

let sendDirectMessage = ()=>{
  let _user_id = JSON.parse(sessionStorage.getItem('access_token')).user_id;
  let _content = document.getElementById('message-input');
  let _to = null;

  if(ActiveMessage.get()){
    ActiveMessage.get()[0].parties.forEach((item,key)=>{
      if(item != _user_id){
        _to = item;
      }
    });

    Meteor.call('sendDirectMessage',_content.value,_to,sessionStorage.getItem('access_token'),ActiveMessage.get()[0]._id,(err,res)=>{
      _content.value = "";

      Tracker.afterFlush(()=>{
        let _someItem = $('.message-area .content');
        _someItem.scrollTop(_someItem.prop("scrollHeight"));
      });
    });
  }
}
