import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

import './message-nav.html';

Template.messageNav.onCreated(()=>{

});

Template.messageNav.events({
  'click #open-conversation-list'(event){
    let _conversation = document.getElementsByClassName('conversation-mobile');
    let _message_nav = document.getElementsByClassName('message-navigation');

    if(_conversation[0]){
      _conversation[0].style.display = 'block';
    }

    _message_nav[0].style.display = 'none';
  },

  'click #close-message-navigation'(event){
    let _message_nav = document.getElementsByClassName('message-navigation');

    _message_nav[0].style.display = 'none';
  }
});

Template.messageNav.helpers({

});
