import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './register.html';

User = new ReactiveVar(null);

Template.register.onCreated(()=>{

});

Template.register.events({
  'click #register-button'(event){
    let user_data = {
      "email": document.getElementById('email').value,
      "first_name": document.getElementById('first-name').value,
      "last_name": document.getElementById('last-name').value,
      "password": document.getElementById('password').value
    }

    Meteor.call('userRegister',user_data,(err,res)=>{
      if(res){
        ToasterMessage.set('Register Successful');
        ToasterState.set('active');
        ToasterType.set('success');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);
          ToasterType.set(null);

          FlowRouter.go('/');
        },6000);
      }else{
        ToasterMessage.set('Register Unsuccessful');
        ToasterState.set('active');
        ToasterType.set('failed');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);
          ToasterType.set(null);

          FlowRouter.go('/register');
        },6000);
      }
    });
  }
});

Template.register.helpers({

});
