import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './contact.html';

import './contact-list/contact-list';
import './contact-add/contact-add';

Template.contact.onCreated(()=>{

});

Template.contact.events({
  'click #contact-add'(event){
    if(CreateContactToggle.get() == false){
      CreateContactToggle.set(true);
      ModalCurtainToggle.set(true);
    }else{
      CreateContactToggle.set(false);
      ModalCurtainToggle.set(false);
    }
  }
});

Template.contact.helpers({
  showCreateContact: function(){
    return CreateContactToggle.get();
  }
});
