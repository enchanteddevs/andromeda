import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './contact-add.html';

let AvailableUsers = new ReactiveVar(null);

Template.contactAdd.events({
  'click #close-contact-create-modal'(event){
    if(CreateContactToggle.get() == false){
      CreateContactToggle.set(true);
      ModalCurtainToggle.set(true);
    }else{
      CreateContactToggle.set(false);
      ModalCurtainToggle.set(false);
      AvailableUsers.set([]);
    }
  },

  'change #search-contact-to-add'(event){
    Meteor.call('findAvailableUser',event.currentTarget.value,(err,res)=>{
      AvailableUsers.set(res);
    });
  },

  'click .contact-create-list > li'(event){
    if(event.currentTarget.children[1].children[0].children[0].className == 'icon-checked'){
      event.currentTarget.children[1].children[0].children[0].className = 'icon-unchecked';
    }else{
      event.currentTarget.children[1].children[0].children[0].className = 'icon-checked';
    }
  },

  'click #btn-add-contact'(event){
    let user_list = document.querySelector(".contact-create-list");
    let selected_user = [];

    for(let i=0;i<user_list.children.length;i++){
      let checkbox = user_list.children[i].querySelector('.checkbox > h5 > span');
      let user_id = user_list.children[i];

      if(checkbox.className == 'icon-checked'){
        selected_user.push(user_id.dataset.val);
      }
    }

    //Add users to contact list
    Meteor.call('addContact',selected_user,sessionStorage.getItem('access_token'),(err,res)=>{
      //Close modal
      if(CreateContactToggle.get() == false){
        CreateContactToggle.set(true);
        ModalCurtainToggle.set(true);
      }else{
        CreateContactToggle.set(false);
        ModalCurtainToggle.set(false);
      }

      //Check server response
      if(res){
        ToasterMessage.set('Add Contact Successful');
        ToasterState.set('active');
        ToasterType.set('success');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);
          AvailableUsers.set(null);

          loadContact();
        },6000);
      }else{
        ToasterMessage.set('Add Contact Unsuccessful');
        ToasterState.set('active');
        ToasterType.set('failed');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);
          AvailableUsers.set(null);
        },6000);
      }
    });
  }
});

Template.contactAdd.helpers({
  contactCreateList: function(){
    return AvailableUsers.get();
  }
});
