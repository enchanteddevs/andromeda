import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './contact-list.html';

import './../contact-detail/contact-detail';

Template.contactList.onCreated(()=>{

});

Template.contactList.helpers({
  contacts: ()=>{
    let contacts = Contact.find().fetch();

    if(contacts[0]){
      return contacts[0].contacts;
    }else{
      return [];
    }
  }
});

Template.contactList.events({
  'click .remove-contact-btn'(event){
    let user_id = event.currentTarget.dataset.val;

    Meteor.call('removeContact',user_id,sessionStorage.getItem('access_token'),(err,res)=>{
      if(res){
        //loadContact();
      }else{

      }
    });
  },

  'click .message-contact-btn'(event){
    let user_id = event.currentTarget.dataset.val;

    Meteor.call('startConversation',sessionStorage.getItem('access_token'),user_id,(err,res)=>{
      if(res){
        ActiveMessage.set(res);

        FlowRouter.go('/message');
      }
    });
  }
})
