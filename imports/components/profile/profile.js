import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

import './profile.html';

Template.profile.onCreated(()=>{
  subscribeToProfile();
});

Template.profile.events({

});

Template.profile.helpers({
  profile: ()=>{
    let _profile = Profile.find().fetch();

    if(_profile){
      return _profile[0];
    }
  }
});
