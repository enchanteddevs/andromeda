import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

import './contact-mobile.html';

Template.contactMobile.helpers({
  contacts: ()=>{
    let contacts = Contact.find().fetch();

    if(contacts[0]){
      return contacts[0].contacts;
    }else{
      return [];
    }
  }
});

Template.contactMobile.events({
  'click .contact-mobile-action-message > a'(event){
    let user_id = event.currentTarget.dataset.val;

    Meteor.call('startConversation',sessionStorage.getItem('access_token'),user_id,(err,res)=>{
      if(res){
        ActiveMessage.set(res);

        FlowRouter.go('/message');
      }
    });
  },

  'click .contact-mobile-action-remove > a'(event){
    let user_id = event.currentTarget.dataset.val;

    Meteor.call('removeContact',user_id,sessionStorage.getItem('access_token'),(err,res)=>{
      if(res){
        //loadContact();
      }else{

      }
    });
  }
});
