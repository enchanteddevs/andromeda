import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'reactive-var';

import './login.html';

Template.login.events({
  'click #login-button'(event){
    let username = document.getElementById('email').value;
    let password = document.getElementById('password').value;

    Meteor.call('userLogin',username,password, (err,res) => {
      if(res){
        ToasterMessage.set('Login Successful');
        ToasterState.set('active');
        ToasterType.set('success');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);

          //Store token in session
          sessionStorage.setItem('access_token',JSON.stringify(res));

          FlowRouter.go('/contact');
        },6000);
      }else{
        ToasterMessage.set('Login Unsuccessful');
        ToasterState.set('active');
        ToasterType.set('failed');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);

          FlowRouter.go('/');
        },6000);
      }
    });
  }
});

Template.login.helpers({

});
