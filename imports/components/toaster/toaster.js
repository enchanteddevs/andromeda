import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

import './toaster.html';

ToasterMessage = new ReactiveVar(null);
ToasterState = new ReactiveVar(null);
ToasterType = new ReactiveVar(null);

Template.toaster.onCreated(()=>{});

Template.toaster.helpers({
  toaster_message: function(){
    return ToasterMessage.get();
  },

  toaster_state: function(){
    return ToasterState.get();
  },

  toaster_type: function(){
    return ToasterType.get();
  }
});
