import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {Mongo} from 'meteor/mongo';

import './conversation.html';

Template.conversation.onCreated(()=>{

});

Template.conversation.helpers({
  conversations: ()=>{
    return Conversation.find().fetch();
  },

  conversation_with: (parties)=>{
    let token = JSON.parse(sessionStorage.getItem('access_token'));
    let _contact = Contact.findOne({user_id: token.user_id});

    if(parties[0] == token.user_id){
      _contact = getContactProfileById(parties[1],_contact.contacts);

      return _contact.first_name;
    }else{
      _contact = getContactProfileById(parties[0],_contact.contacts);

      return _contact.first_name;
    }
  }
});

Template.conversation.events({
  'click .conversation-contact'(event){
    let _conversation_id = event.currentTarget.dataset.val;

    //Activate conversation
    let _conversation = Conversation.find({_id: _conversation_id}).fetch();

    if(_conversation){
      ActiveMessage.set(_conversation);

      Tracker.afterFlush(()=>{
        let _someItem = $('.message-area .content');
        _someItem.scrollTop(_someItem.prop("scrollHeight"));
      });
    }
  }
});
