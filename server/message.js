import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.publish('messages',(token)=>{
  token = JSON.parse(token);

  return Message.find({$or:[{to:token.user_id},{owner:token.user_id}]});
});

Meteor.methods({
  activateMessage: (user_id,token) =>{
    //Parse token to JSON
    token = JSON.parse(token);

    //Deactivate other active messages
    let message_objects = Message.find({"from": token.user_id}).fetch();
  },

  sendDirectMessage: (content,to,token,conversation_id) => {
    token = JSON.parse(token);

    if(conversation_id && token){
      let _status = Message.insert({
        'to': to,
        'owner': token.user_id,
        'conversation_id': conversation_id,
        'content': content,
        'type': 'text',
        'timestamp': Date.now()
      });

      if(_status){
        return true;
      }else{
        return false;
      }
    }
  }
});
