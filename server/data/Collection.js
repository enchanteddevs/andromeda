import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

User = new Mongo.Collection('user');

Contact = new Mongo.Collection('contact');

Channel = new Mongo.Collection('channel');
Conversation = new Mongo.Collection('conversation');
Message = new Mongo.Collection('message');

AccessTokenData = new Mongo.Collection('access_token');
