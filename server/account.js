import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

import {AccessToken} from './modules/AccessToken/AccessToken';

let access_token = null;

Meteor.startup(()=>{
  access_token = new AccessToken();
});

Meteor.methods({
  userLogin: (username, password) => {
    let user = User.find({"email":username,"password":password}).fetch();

    if(user.length > 0){
      return access_token.generateToken(user[0]._id);
    }else{
      return false;
    }
  },

  userLogout: (token) => {
    console.log('logging out...');
    return access_token.removeToken(JSON.parse(token));
  },

  userRegister: (user_data) => {
    //Check if user exist
    let user = User.find({"email":user_data.email}).fetch();

    if(user.length <= 0){
      User.insert({
        "email": user_data.email,
        "first_name": user_data.first_name,
        "last_name": user_data.last_name,
        "password": user_data.password,
        "created_date": Date()
      });

      return true;
    }else{
      return false;
    }
  },

  userForgotPassword: () => {

  },

  userProfile: () => {

  }
})
