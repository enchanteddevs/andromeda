import {CustomHashLib} from './../../enum/CustomHashLib';

export class AccessToken{
  generateToken(user_id){
    let index = Math.floor((Math.random() * 10) + 1); //Return random number between 1 to 10
    let token_str = (new Date()).getTime() + CustomHashLib[index];
    let datetime = new Date();

    let existing_token = this.findToken(user_id);

    if(existing_token == null){
      AccessTokenData.insert({
        'user_id': user_id,
        'token' : token_str,
        'created_at': datetime
      });

      return this.findToken(user_id);
    }else{
      return existing_token;
    }
  }

  findToken(user_id){
    let token = AccessTokenData.find({'user_id': user_id}).fetch();

    if(token.length > 0){
      return token[0];
    }else{
      console.log('none');
      return null;
    }
  }

  validateToken(access_token){
    let create_at = new Date(access_token.created_at);

  }

  removeToken(access_token){
    let token = AccessTokenData.remove({"_id": access_token._id});

    if(token == 1){
      console.log('token removed');
      return true;
    }else{
      return false;
    }
  }
}
