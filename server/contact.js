import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.publish('contacts',(token)=>{
  token = JSON.parse(token);

  return Contact.find({"user_id": token.user_id});
});

Meteor.methods({
  addContact: (users,token) => {
    //Parse token to JSON
    token = JSON.parse(token);

    //Get member contacts
    let member_contacts = Contact.findOne({"user_id": token.user_id});

    if(typeof member_contacts === 'undefined'){
      Contact.insert({
        "user_id": token.user_id,
        "contacts": []
      });

      member_contacts = Contact.findOne({"user_id": token.user_id});
    }

    try{
      let contact_to_be_added = [];

      for(let i = 0; i<users.length;i++){
        //Get contact data
        let user = User.findOne({"_id":users[i]});

        //Check if contact has already been added
        if(member_contacts.contacts.length > 0){
          let exist_flag = false;

          for(let j=0; j<member_contacts.contacts.length; j++){
            if(user._id == member_contacts.contacts[j]._id){
              exist_flag = true;
            }
          }

          if(exist_flag == false){
            //Add to contact
            contact_to_be_added.push(user);
          }
        }else{
          //Add to contact
          contact_to_be_added.push(user);
        }
      }

      //Append the contact list with the new one
      console.log(contact_to_be_added);
      for(let i = 0; i < contact_to_be_added.length; i++){
        member_contacts.contacts.push(contact_to_be_added[i]);
      }

      //Update member contact
      let status = Contact.update({"_id": member_contacts._id},member_contacts);

      return true;
    }catch(ex){
      console.log(ex);
      return false;
    }
  },

  removeContact: (user_id,token) => {
    //Parse token to JSON
    token = JSON.parse(token);

    let member_contact = Contact.findOne({"user_id": token.user_id});

    if(typeof member_contact !== 'undefined'){
      let contacts = member_contact.contacts;

      contacts.forEach((item,key) => {
        if(item._id == user_id){
          member_contact.contacts.splice(key,1);
        }
      });

      Contact.update({"user_id":token.user_id},member_contact);

      return true;
    }else{
      return false;
    }
  },

  getInfoById: (user_id, token) => {
    token = JSON.parse(token);

    let contact_obj = Contact.findOne({"user_id": token.user_id});
    let contact_info = null;

    contact_obj.contacts.forEach((item,val)=>{
      if(item._id == user_id){
        contact_info = item;

        return true;
      }
    });

    if(contact_info != null){
      console.log(contact_info);
      return contact_info;
    }

    return false;
  }
});
