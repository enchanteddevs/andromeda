import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.publish('profile',(token)=>{
  token = JSON.parse(token);
  console.log(token);
  if(token){
    return User.find({_id: token.user_id});
  }else{
    return null;
  }
});

Meteor.methods({
  findAvailableUser: (phrase) => {
    let user = User.find({"email":  {$regex : phrase}}).fetch();

    return user;
  },

  loadUserProfile: (user_id) => {
    let user = User.findOne({"_id": user_id});

    if(user){
      return user;
    }else{
      return null;
    }
  }
});
