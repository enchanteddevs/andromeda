import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.publish('conversations',(token)=>{
  //Convert token to JSON object
  token = JSON.parse(token);

  //Check if conversation exists
  let _conversations = Conversation.find({
    "parties": {
      $in:[
        token.user_id
      ]
    }
  });

  return _conversations;
});

Meteor.methods({
  startConversation: (token,target) =>{
    //Convert token to JSON object
    token = JSON.parse(token);

    //Check if conversation exists
    let _conversations = Conversation.find({
      "parties": {
        $all:[
          token.user_id,
          target
        ]
      }
    }).fetch();

    if(_conversations && _conversations.length > 0){
      console.log(_conversations);
      return _conversations;
    }else{
      console.log("Create conversation");
      //Create a new conversation
      let _status = Conversation.insert({
        "parties": [
          token.user_id,
          target
        ]
      });

      //Return the newly created conversation
      /*return  Conversation.findOne({
        "parties": {
          $in:[
            token.user_id,
            target
          ]
        }
      });*/
      return Conversation.findOne({_id: _status});
    }
  }
})
