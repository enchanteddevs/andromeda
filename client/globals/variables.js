import {Meteor} from 'meteor/meteor';
import {ReactiveVar} from 'meteor/reactive-var';
import {Mongo} from 'meteor/mongo';

Profile = new ReactiveVar(null);
Contacts = new ReactiveVar(null);
ActiveMessage = new ReactiveVar(null);
Messages = new ReactiveVar(null);
//ContactMessages = new ReactiveVar(null);

CreateContactToggle = new ReactiveVar(false);
ModalCurtainToggle = new ReactiveVar(false);
