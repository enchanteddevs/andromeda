import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';

getContactProfileById = (id,contacts)=>{
  let _profile = null;

  contacts.forEach((item,key)=>{
    if(id == item._id){
      _profile = item;
    }
  });

  return _profile;
}

getConversationWith = (parties) =>{
  let token = JSON.parse(sessionStorage.getItem('access_token'));
  let _contact = Contact.findOne({user_id: token.user_id});

  if(parties[0] == token.user_id){
    _contact = getContactProfileById(parties[1],_contact.contacts);
    console.log(_contact);
    return _contact.first_name;
  }else{
    _contact = getContactProfileById(parties[0],_contact.contacts);
    console.log(_contact);
    return _contact.first_name;
  }
}

resetAllGlobal = (cb) =>{
  Profile.set(null);
  Contacts.set(null);
  ActiveMessage.set(null);
  Messages.set(null);

  cb(null,true);
}

subscribeToContact = ()=>{
  Meteor.subscribe('contacts',sessionStorage.getItem('access_token'));
}

subscribeToMessage = ()=>{
  Meteor.subscribe('messages',sessionStorage.getItem('access_token'));
}

subscribeToProfile = ()=>{
  Meteor.subscribe('profile',sessionStorage.getItem('access_token'));
}

subscribeToConversation = ()=>{
  Meteor.subscribe('conversations',sessionStorage.getItem('access_token'));
}
