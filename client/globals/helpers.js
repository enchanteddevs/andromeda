import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

Template.registerHelper('isEqual',(a,b)=>{
  if(a == b){
    return true;
  }else{
    return false;
  }
});

Template.registerHelper('modalCurtain',()=>{
  return ModalCurtainToggle.get();
});

Template.registerHelper('me',()=>{
  return JSON.parse(sessionStorage.getItem('access_token'));
});

Template.registerHelper('userFirstName',(user_id)=>{
  Meteor.call('loadUserProfile',user_id,(err,res)=>{
    if(res){
      return res.first_name;
    }
  });
});
