import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';

Template.body.events({
  'click .modal-curtain'(event){
    ModalCurtainToggle.set(false);
    console.log('curtain');
    let _burger_menu_list = document.getElementById('burger-menu-list');
    _burger_menu_list.style.display = 'none';
  }
});
