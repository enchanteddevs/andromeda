import {Meteor} from 'meteor/meteor';

FlowRouter.route('/contact',{
  action: function(){
    if(sessionStorage.getItem('access_token') != 'null'){
      BlazeLayout.render('memberLayout',{content: 'contact'});
    }else{
      FlowRouter.go('/');
    }
  }
});

FlowRouter.route('/message',{
  action: function(){
    if(sessionStorage.getItem('access_token') != 'null'){
      BlazeLayout.render('memberLayout',{content: 'message'});
    }else{
      FlowRouter.go('/');
    }
  }
});

FlowRouter.route('/profile',{
  action: function(){
    if(sessionStorage.getItem('access_token') != 'null'){
      BlazeLayout.render('memberLayout',{content: 'profile'});
    }else{
      FlowRouter.go('/');
    }
  }
});
