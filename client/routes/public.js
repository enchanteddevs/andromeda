import {Meteor} from 'meteor/meteor';

FlowRouter.route('/',{
  action: function(){
    BlazeLayout.render('publicLayout',{content: 'login'});
  }
});

FlowRouter.route('/register',{
  action: function(){
    BlazeLayout.render('publicLayout',{content: 'register'});
  }
});

FlowRouter.route('/forgot-password',{
  action: function(){
    BlazeLayout.render('publicLayout',{content: 'forgotPassword'});
  }
});
