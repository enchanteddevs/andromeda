import {Meteor} from 'meteor/meteor';

import './../imports/layout/member_layout.html';
import './../imports/layout/public_layout.html';

import './../imports/components/register/register';
import './../imports/components/login/login';
import './../imports/components/forgot-password/forgot-password';
import './../imports/components/toaster/toaster';
import './../imports/components/contact/contact';
import './../imports/components/contact-mobile/contact-mobile';
import './../imports/components/message/message';
import './../imports/components/profile/profile';

Template.memberLayout.onCreated(()=>{
  subscribeToContact();
  subscribeToConversation();
  subscribeToMessage();
});

Template.memberLayout.events({
  'click .logout-nav'(event){
    Meteor.call('userLogout',sessionStorage.getItem('access_token'),(err,res) => {
      if(res){
        ToasterMessage.set('Logout Successful');
        ToasterState.set('active');
        ToasterType.set('success');

        setTimeout(function(){
          resetAllGlobal((err,res) =>{
            ToasterState.set(null);
            ToasterMessage.set(null);

            sessionStorage.setItem('access_token',null);

            //window.location.href = '/';
            FlowRouter.go('/');
          });
        },6000);
      }else{
        ToasterMessage.set('Logout Unsuccessful');
        ToasterState.set('active');
        ToasterType.set('failed');

        setTimeout(function(){
          ToasterState.set(null);
          ToasterMessage.set(null);
        },6000);
      }
    });
  },

  'click #burger-menu'(event){
    ModalCurtainToggle.set(true);

    let _burger_menu_list = document.getElementById('burger-menu-list');
    _burger_menu_list.style.display = 'block';
  },

  'blur #burger-menu-list'(event){
    let _burger_menu_list = document.getElementById('burger-menu-list');
    _burger_menu_list.style.display = 'none';
  },

  'click #burger-menu-list a'(event){
    ModalCurtainToggle.set(false);
    console.log('curtain');
    let _burger_menu_list = document.getElementById('burger-menu-list');
    _burger_menu_list.style.display = 'none';
  }
});
