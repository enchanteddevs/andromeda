import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.subscribe('conversations',sessionStorage.getItem('access_token'));

Conversation = new Mongo.Collection('conversation');
