import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.subscribe('contacts',sessionStorage.getItem('access_token'));

Contact = new Mongo.Collection('contact');
