import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';

Meteor.subscribe('profile',sessionStorage.getItem('access_token'));

Profile = new Mongo.Collection('user');
